import { ApolloClient, InMemoryCache } from "@apollo/client";

export const client = new ApolloClient({
  uri:
    process.env.NODE_ENV === "development"
      ? "http://localhost:3100/graphql"
      : "https://threecd-gateway.herokuapp.com/graphql",
  cache: new InMemoryCache(),
});
