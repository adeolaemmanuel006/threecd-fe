import { isScreenPort } from "../scripts/utils";

const servicesCard = {
  initial: {
    opacity: 0,
  },
  animate: {
    opacity: 1,

    transition: {
      duration: 0.2,
      delay: 0.3,
    },
  },

  exit: {
    x: "-100%",
    transition: { ease: "easeInOut" },
  },
};

const text_slideUp_animation = {
  initial: {
    opacity: 0,
    y: "-100%",
  },
  animate: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.5,
      delay: 0.5,
    },
  },

  exit: {
    x: "-100%",
    transition: { ease: "easeInOut", delay: 0.7 },
  },
};

const summary_animation = {
  initial: {
    opacity: 0,
    // y: "-100%",
  },
  animate: {
    opacity: 1,
    // y: 0,
    transition: {
      duration: 1,
      delay: 1,
    },
  },

  exit: {
    x: "-100%",
    transition: { ease: "easeInOut", delay: 0.7 },
  },
};

export { servicesCard, text_slideUp_animation, summary_animation };
