export const isScreenPort = (port: number): boolean => {
  typeof window !== "undefined" &&
    window.addEventListener("resize", () => {
      typeof window !== "undefined" && window.location.reload();
    });
  if (
    typeof window !== "undefined" &&
    window.matchMedia(`(max-width: ${port}px)`).matches
  ) {
    return true;
  } else {
    return false;
  }
};
