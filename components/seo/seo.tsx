import Head from "next/head";

const SEO: React.FC<{title?: string}> = ({ children, title }) => {
  return (
    <Head>
      <title>{title}</title>
      {children}
    </Head>
  );
};

export default SEO;
