import CSS from "csstype";
export interface ButtonI {
  className?: string;
  style?: CSS.Properties;
  width?: number;
  height?: number;
  click?: () => void;
  variant?: {};
  type?: "button" | "reset" | "submit";
  loading?: boolean
}
