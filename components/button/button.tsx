import { motion } from "framer-motion";
import Loading from "../loading/loading";
import { ButtonI } from "./Button";
import styles from "./button.module.css";

const Button: React.FC<ButtonI> = ({
  children,
  className,
  click,
  variant,
  type = "button",
  style,
  loading,
}) => {
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <motion.button
          variants={variant}
          initial="initial"
          animate="animate"
          type={type}
          onClick={click}
          className={`btn ${className}`}
          style={style}
        >
          {children}
        </motion.button>
      )}
    </>
  );
};

export default Button;
