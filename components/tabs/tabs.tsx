import { Col, Row } from "reactstrap";
import Button from "../button/button";
import { TabsI } from "./Tab";
import styles from "./tabs.module.css";

const Tabs: React.FC<TabsI> = ({ tabs, active, setTab, className }) => {
  return (
    <Row
      className={`mx-auto theme pt-2 pb-2 justify-content-between ${styles.tabs_con} ${className}`}
    >
      {tabs?.map((tab) => {
        if (tab === active) {
          return (
            <Col key={tab}>
              <Button
                click={() => setTab && setTab(tab)}
                className={`bg-white w-100 ${styles.btn}`}
              >
                {tab}
              </Button>
            </Col>
          );
        } else {
          return (
            <Col key={tab}>
              <Button
                click={() => setTab && setTab(tab)}
                className={`text-white w-100 ${styles.btn}`}
              >
                {tab}
              </Button>
            </Col>
          );
        }
      })}
    </Row>
  );
};

export default Tabs;
