export enum Tab {
  cons = "CONSTRUCTION",
  consl = "CONSULTANT",
  conrt = "CONTRACTOR",
}

export interface TabsI {
  active?: string;
  tabs?: string[];
  setTab?: (tab: any) => void;
  className?: string;
}
