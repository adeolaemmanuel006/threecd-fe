import { motion } from "framer-motion";
import Image from "next/image";
import { AboutModal, EditAboutModal } from "../../container/About/about";
import { AdminModal } from "../../container/Admin/about/about";
import { EditProjects } from "../../container/Admin/projetcs/projetcs";
import { ServicesEdit } from "../../container/Admin/services/services";
import { ProjectsModal } from "../../container/Projects/projects";
import { setModal } from "../../store/reducers/pageSlice";
import { useAppDispatch, useAppSelector } from "../../store/store";
import Button from "../button/button";
import { ModalI } from "./@types";
import styles from "./modal.module.css";

const PopUpModal: React.FC<{
  component?: string;
  data?: string;
  close: () => void;
}> = ({ component, data, close }) => {
  console.log(component, data);

  return (
    <motion.div
      initial={{ scale: 0 }}
      animate={{ scale: 1 }}
      className={`${styles.popup_modal}`}
    >
      <div className={`${styles.inner_popup_modal_con}`}>
        <img
          src="/img/close.svg"
          width={30}
          height={30}
          className={`${styles.close}`}
          onClick={close}
        />

        {component === "AdminAbout" && <AdminModal type={data as string} />}
        {component === "EditAboutModal" && <EditAboutModal />}
        {component === "ServicesEdit" && <ServicesEdit />}
        {component === "EditProjects" && <EditProjects />}
      </div>
    </motion.div>
  );
};
const SideModal: React.FC<{
  component?: string;
  data?: string;
  close: () => void;
}> = ({ component, data, close }) => {
  return (
    <motion.div
      initial={{ x: "100%" }}
      animate={{ x: 0 }}
      transition={{ stiffness: 0, bounceStiffness: 0 }}
      className={`${styles.side_modal}`}
    >
      <>
        <img
          src="/img/close.svg"
          width={30}
          height={30}
          className={`${styles.close}`}
          onClick={close}
        />
        {component === "AboutModal" && <AboutModal />}
        {component === "EditAboutModal" && <EditAboutModal />}
        {component === "ProjectModal" && <ProjectsModal />}
      </>
    </motion.div>
  );
};

const Modal: React.FC<ModalI> = ({ type, component }) => {
  const dispatch = useAppDispatch();
  const modal = useAppSelector((state) => state.page.modal);

  const closeModal = () => {
    dispatch(setModal({ isOpen: false }));
  };
  return (
    <>
      <div
        className={`${styles.modal_con}`}
        style={{ cursor: "pointer" }}
        onClick={closeModal}
      ></div>
      {type === "standard" ? (
        <PopUpModal
          component={modal.component}
          data={modal.data}
          close={closeModal}
        />
      ) : (
        <SideModal
          component={modal.component}
          data={modal.data}
          close={closeModal}
        />
      )}
    </>
  );
};

export default Modal;
