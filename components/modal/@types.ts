export interface ModalI {
  type?: string;
  component?: string;
  data?: string;
}
