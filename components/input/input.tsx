import { InpitI } from "./InputI";
import { useState } from "react";
// import { EditorProps } from "react-draft-wysiwyg";
// import { EditorState } from "draft-js";
import dynamic from "next/dynamic";
import Button from "../button/button";

const Input: React.FC<InpitI> = ({
  type,
  textChange,
  className,
  style,
  placeholder,
  name,
  selectData,
  required,
  value,
  label,
  labelClass,
  maxLength,
  getUploads,
}) => {
  const input = ["text", "number", "date", "password"];
  const [fileUplaods, setFileUplaods] = useState<any[]>([]);
  const [uploadCount, setUploadCount] = useState(fileUplaods.length + 1);
  const [isUploading, setIsUploading] = useState(false);
  // const Editor = dynamic<EditorProps>(
  //   () => import("react-draft-wysiwyg").then((mod) => mod.Editor),
  //   { ssr: false }
  // );
  // const [textArea, setTextArea] = useState(EditorState.createEmpty());

  const uplaod = async () => {
    if (uploadCount !== 0) setIsUploading(true);
    const body = new FormData();
    const allImages = [];
    body.append("upload_preset", "i5n0oohh");
    let data;
    for (const img of fileUplaods) {
      body.append("file", img);
      const res = await fetch(
        `https://api.cloudinary.com/v1_1/${"neutron360"}/image/upload`,
        {
          method: "POST",
          body,
        }
      );
      data = await res.json();
      if (data.url) {
        setUploadCount(uploadCount - 1);
        if (uploadCount === 0) {
          setIsUploading(false);
        }
        allImages.push(data.url);
      }
    }
    if (getUploads) getUploads(allImages);
  };
  return (
    <>
      {type !== "file" && label && (
        <label className={labelClass} htmlFor={name}>
          {label}
        </label>
      )}
      {input.indexOf(type) !== -1 && (
        <input
          type={type}
          name={name}
          onChange={(e) => textChange && textChange(e.target.value)}
          className={className}
          placeholder={placeholder}
          style={{ ...style }}
          required={required}
          value={value}
          id={name}
          maxLength={maxLength}
        />
      )}
      {type === "textarea" && (
        // <Editor
        //   editorState={textArea}
        //   editorClassName={`w3-border`}
        //   editorStyle={{ height: 200 }}
        //   wrapperClassName={`w3-margin-top ${className}`}
        //   onEditorStateChange={(e) => {
        //     console.log(e);
        //     setTextArea(e);
        //   }}
        // />
        <textarea
          className={className}
          name={name}
          onChange={(e) => textChange && textChange(e.target.value)}
          placeholder={placeholder}
          style={{ ...style }}
          required={required}
          value={value}
          id={name}
          maxLength={maxLength}
        ></textarea>
      )}
      {type === "file" && (
        <div className="w3-center">
          <input
            type={type}
            id={name}
            onChange={(e: any) => textChange && textChange(e.target.files[0])}
            hidden
            name={name}
          />
          <label className={`w3-btn ${className}`} htmlFor={name}>
            {placeholder}
          </label>
        </div>
      )}
      {type === "fileUpload" && (
        <div className="w3-center">
          {fileUplaods.map((data) => {
            console.log(data.name);

            return <img src={`/${data.name}`} key={data.name} />;
          })}
          <input
            type="file"
            id={name}
            onChange={(e: any) => {
              const files = [...fileUplaods];
              files.push(e.target.files[0]);
              setUploadCount(files.length);
              if (files.length <= 4) setFileUplaods(files);
            }}
            hidden
            name={name}
          />
          <label className={`w3-btn ${className}`} htmlFor={name}>
            {placeholder}
          </label>
          <div className="w3-center">
            <Button
              className={`w3-btn ${className} theme`}
              click={() => uplaod()}
            >
              {isUploading && uploadCount != 0
                ? `${uploadCount} Files uploading`
                : "Uplaoded"}
            </Button>
          </div>
        </div>
      )}
      {type === "select" && (
        <select
          onChange={(e) => textChange && textChange(e.target.value)}
          className={className}
          style={{ ...style }}
          required={required}
          value={value}
          defaultValue={selectData && selectData[0].value}
          id={name}
        >
          {selectData?.map((data, ind) => {
            if (ind === 0) {
              return (
                <option key={data.value} value={data.value}>
                  {data.name}
                </option>
              );
            } else {
              return (
                <option key={data.value} value={data.value}>
                  {data.name}
                </option>
              );
            }
          })}
        </select>
      )}
    </>
  );
};

export default Input;
