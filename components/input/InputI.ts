export interface InpitI {
  type: string;
  textChange?: (text?: any) => void;
  className?: string;
  style?: {};
  value?: string;
  placeholder?: string;
  label?: string;
  name?: string;
  selectData?: { value: string; name: string }[];
  required?: boolean;
  component?: (data?: any) => JSX.Element | JSX.Element[];
  labelClass?: string;
  maxLength?: number;
  getUploads?: (uploads: string[]) => void;
}
