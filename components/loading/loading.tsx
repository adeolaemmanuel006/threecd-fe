import * as React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";

const Loading = () => {
  return (
    <Box sx={{ display: "flex" }}>
      <CircularProgress
        sx={{
          color: "#264574",
          margin: 'auto',
          display: 'block',
          marginTop: 10
        }}
      />
    </Box>
  );
};

export default Loading;
