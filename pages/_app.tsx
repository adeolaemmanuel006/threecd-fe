import "../styles/globals.css";
import type { AppProps } from "next/app";
import Layout from "../container/Layout/layout";
import { Provider } from "react-redux";
import { store } from "../store/store";
import 'bootstrap/dist/css/bootstrap.min.css';
import { ApolloProvider } from "@apollo/client";
import { client } from "../apolloClient";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Provider>
    </ApolloProvider>
  );
}

export default MyApp;
