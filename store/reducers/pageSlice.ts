import { createSlice } from "@reduxjs/toolkit";
import { Tab } from "../../components/tabs/Tab";
import {
  modalHandler,
  navHandler,
  setAdminHandler,
} from "../actions/pageAction";
import { RootState } from "../store";
import { PageState } from "./@types";

const initialState: PageState = {
  aboutSection: [
    { header: "About", subHeader: "All you need to know about us" },
    {
      header: "Team Members",
      subHeader: "A loyalty to be delivered",
    },
  ],
  serviceTab: [{ type: Tab.cons }, { type: Tab.conrt }, { type: Tab.consl }],
  admin: false,
  modal: {
    isOpen: false,
    type: "standard",
  },
  navIsOpen: false,
};

const pageSlice = createSlice({
  name: "about",
  initialState,
  reducers: {
    setAdmin: (state, payload) => setAdminHandler(state, payload),
    setModal: (state, payload) => modalHandler(state, payload),
    setNav: (state, payload) => navHandler(state, payload),
  },
});

export default pageSlice.reducer;
export const selectAbout = (state: RootState) => state.page;
export const { setAdmin, setModal, setNav } = pageSlice.actions;
