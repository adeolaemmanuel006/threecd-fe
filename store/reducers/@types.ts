import React from "react";
import { Tab } from "../../components/tabs/Tab";

type modalType = "standard" | "sidemodal";

interface Modal {
  isOpen: boolean;
  type: modalType;
  component?: string;
  data?: string;
  props?: {};
}

export interface PageState {
  aboutSection: { header: string; subHeader: string }[];
  serviceTab: { type: Tab }[];
  modal: Modal;
  admin: boolean;
  user?: { email: string; isAdmin: boolean };
  navIsOpen: boolean
}
