import { PageState } from "../reducers/@types";

const setAdminHandler = (state: PageState, payload: any) => {
  state.admin = payload.payload;
};

const modalHandler = (state: PageState, payload: any) => {
  state.modal.type = payload.payload.type || "standard";
  state.modal.component = payload.payload.component || "";
  state.modal.isOpen = payload.payload.isOpen || false;
  state.modal.data = payload.payload.data || "";
  state.modal.props = payload.payload.props || {};
};

const navHandler = (state: PageState, payload: any) => {
  state.navIsOpen = !payload.payload.navIsOpen;
};

export { setAdminHandler, modalHandler, navHandler };
