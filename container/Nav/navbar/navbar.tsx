import { Button, Typography } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useAppSelector } from "../../../store/store";
import EmailIcon from "@mui/icons-material/Email";
import CallIcon from "@mui/icons-material/Call";
import styles from "./nav.module.css";
import { isScreenPort } from "../../../assets/scripts/utils";
import { NavBarI } from "./Navbar";

const NavBar: React.FC<NavBarI> = ({ openSideBar }) => {
  const adminState = useAppSelector((state) => state.page.admin);

  return (
    <div className={`theme flex-row ${styles.nav}`}>
      <div className={`flex-row row-col-5 ${styles.row_col_5 }`}>
        <div className={`theme-3 ${styles.menu}`} onClick={() => openSideBar()}>
          <div className={`${styles.icon}`}>
            <MenuIcon
              sx={{ width: 50, height: 40, color: "white" }}
            />
          </div>
        </div>
      </div>
      <div>
        <Typography
          variant="h6"
          component="div"
          className={`w3-xlarge w3-bold w3-text-white w3-margin-top w3-margin-left`}
        >
          3C-DIMENSION LTD
        </Typography>
      </div>
    </div>
  );
};

export default NavBar;
