import Router from "next/router";
import styles from "./menu.module.css";
import CloseIcon from "@mui/icons-material/Close";
import { MenuI } from "./Menu";
import { meunAnimation } from "../../../assets/animations/menu";
import { AnimatePresence, motion } from "framer-motion";

const Menu: React.FC<MenuI> = ({ isOpen, isOpenHandler, isAdmin }) => {
  const links = [
    { url: isAdmin ? "/admin/dashboard" : "/", name: "HOME" },
    { url: isAdmin ? "/admin/about" : "/about", name: "ABOUT" },
    { url: isAdmin ? "/admin/services" : "/services", name: "SERVICES" },
    { url: isAdmin ? "/admin/projects" : "/projects", name: "PROJECTS" },
    { url: isAdmin ? "/admin/contact" : "/contact", name: "CONTACT" },
    { url: isAdmin ? "/admin/blog" : "/blog", name: "BLOG" },
  ];
  return (
    <>
      {isOpen && (
        <AnimatePresence exitBeforeEnter>
          <motion.div
            variants={meunAnimation}
            initial="initial"
            animate="animate"
            exit="exit"
            className={`${styles.sidebar_con} theme-2`}
          >
            <div
              className={`theme-3 ${styles.menu}`}
              onClick={() => isOpenHandler()}
            >
              <CloseIcon sx={{ width: 50, height: 41, color: "white" }} />
            </div>
            {links.map((l) => {
              return (
                <div
                  key={l.name}
                  className={`${styles.sidebar_item}`}
                  onClick={() => {
                    Router.push(l.url);
                    isOpenHandler();
                  }}
                >
                  <a
                    className={`${styles.sidebar_text}`}
                    onClick={() => {
                      Router.push(l.url);
                      isOpenHandler();
                    }}
                  >
                    {l.name}
                  </a>{" "}
                </div>
              );
            })}
          </motion.div>
        </AnimatePresence>
      )}
    </>
  );
};

export default Menu;
