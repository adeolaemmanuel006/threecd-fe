export interface MenuI {
  isOpen?: boolean;
  isOpenHandler: () => void;
  isAdmin?: boolean;
}
