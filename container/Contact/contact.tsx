import styles from "./contact.module.css";
import Input from "../../components/input/input";
import Button from "../../components/button/button";

const Contact = () => {
  return (
    <div className={`theme-light ${styles.contact_con}`}>
      <div className={`${styles.input_con}`}>
        <form>
          <Input
            type="text"
            placeholder="Firstname:"
            className={`w3-input w3-border w3-round w3-card ${styles.input}`}
          />
          <Input
            type="text"
            placeholder="Lastname:"
            className={`w3-input w3-border w3-round w3-card ${styles.input}`}
          />
          <Input
            type="text"
            placeholder="Email:"
            className={`w3-input w3-border w3-round w3-card ${styles.input}`}
          />
          <Input
            type="text"
            placeholder="Subject:"
            className={`w3-input w3-border w3-round w3-card ${styles.input}`}
          />
          <Input
            type="textarea"
            placeholder="Message:"
            className={`w3-input w3-border w3-round w3-card ${styles.textarea}`}
          />
          <div className="w3-center">
            <Button className="btn-success mt-4 w-25">Submit</Button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default Contact;
