import gql from "graphql-tag";

export type ADD_CONST_DTO = {
  data: {
    image: string;
    summary: string;
    updated_at: string;
    created_at: string;
  };
};

export const ADD_CONST = gql`
  mutation addConst($image: String!, $summary: String!) {
    addConstruction(constInput: { image: $image, summary: $summary }) {
      image
      summary
    }
  }
`;
