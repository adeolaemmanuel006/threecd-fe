import styles from "./services.module.css";
import { Tab } from "../../../components/tabs/Tab";
import { useEffect, useState } from "react";
import { isScreenPort } from "../../../assets/scripts/utils";
import { useAppSelector } from "../../../store/store";
import Input from "../../../components/input/input";
import Button from "../../../components/button/button";
import { useLazyQuery, useMutation } from "@apollo/client";
import Service from "../../Services/services";
import { useDispatch } from "react-redux";
import { setModal } from "../../../store/reducers/pageSlice";
import { Add_Services } from "../../Services/services.gql";
const Services = () => {
  const admin = useAppSelector((state) => state.page.admin);
  const dispatch = useDispatch();

  const AdminEdit = (data: any, type: string) => {
    dispatch(
      setModal({
        props: { ...data, type: type },
        isOpen: true,
        component: "ServicesEdit",
        type: "standard",
      })
    );
  };

  return <Service admin={admin} action={AdminEdit} />;
};

export const ServicesEdit = () => {
  const [addServices, { loading: servicesLoading }] = useMutation(Add_Services);
  const props: any = useAppSelector((state) => state.page.modal.props);
  const [dataForm, setData] =
    useState<{ image: string; summary: string }>(props);

  const submit = async (e: any) => {
    e.preventDefault();
    if (dataForm.summary) {
      const body = new FormData();
      body.append("file", dataForm.image);
      body.append("upload_preset", "qefgkhhb");
      const res = await fetch(
        `https://api.cloudinary.com/v1_1/${"threecdimesion"}/image/upload`,
        {
          method: "POST",
          body,
        }
      );
      const data = await res.json();
      if (data.url) {
        dataForm.image = data.url;
      }
      const response = await addServices({
        variables: {
          type: props.type,
          data: {
            image: dataForm.image,
            summary: dataForm.summary,
          },
        },
      });

      if (response.errors) {
        alert(response.errors);
      } else {
        alert("Data Submited");
      }
    }
  };

  return (
    <form className={`${styles.input_con}`} onSubmit={(e) => submit(e)}>
      <img src={dataForm.image} className={`${styles.img} mb-2`} />
      <Input
        type="file"
        placeholder="Upload"
        value={dataForm.summary}
        className={`w3-btn theme`}
        name="upload"
        textChange={(image) =>
          setData((state) => {
            return { ...state, image };
          })
        }
      />
      <Input
        type="textarea"
        value={dataForm.summary}
        className={`w3-input w3-border w3-round ${styles.input}`}
        textChange={(summary) =>
          setData((state) => {
            return { ...state, summary };
          })
        }
      />
      <div className="w3-center">
        <Button
          loading={servicesLoading}
          type="submit"
          className="btn-success mt-3 w-50"
        >
          Submit
        </Button>
      </div>
    </form>
  );
};

export default Services;
