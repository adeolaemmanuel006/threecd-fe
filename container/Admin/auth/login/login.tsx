import styles from "./login.module.css";
import Input from "../../../../components/input/input";
import Button from "../../../../components/button/button";
import { useForm } from "react-hook-form";
import { useAppDispatch } from "../../../../store/store";
import { setAdmin } from "../../../../store/reducers/pageSlice";
import { useRouter } from "next/router";

const Login = () => {
  const { handleSubmit, register } = useForm();
  const dispatch = useAppDispatch();
  const router = useRouter();

  const submit = (e: any) => {
    e.preventDefault();
    dispatch(setAdmin(true));
    router.push("/admin/projects");
  };

  return (
    <div className={`theme-light ${styles.login_con}`}>
      <div className={`${styles.input_con}`}>
        <form onSubmit={(e) => submit(e)}>
          <Input
            type="text"
            placeholder="Username/Email"
            className={`w3-border w3-round ${styles.input}`}
            required
          />
          <Input
            type="password"
            placeholder="Password"
            className={`w3-border w3-round w3-margin-top ${styles.input}`}
            required
          />
          <Button
            className={`btn btn-success mt-3 w-50 ${styles.btn}`}
            type="submit"
          >
            Login
          </Button>
        </form>
      </div>
    </div>
  );
};
export default Login;
