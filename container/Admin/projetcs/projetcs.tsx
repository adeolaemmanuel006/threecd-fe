import styles from "./projects.module.css";
import Tab from "../../../components/tabs/tabs";
import { useState } from "react";
import New from "./new/new";
import Project from "../../Projects/projects";
import { isScreenPort } from "../../../assets/scripts/utils";
import { useAppSelector } from "../../../store/store";

const Projects: React.FC = () => {
  const [tab, setTab] = useState("PROJETCS");
  const [load, setLoad] = useState(false);
  const admin = useAppSelector((state) => state.page.admin);
  return (
    <div className={`theme-light ${styles.projects_con}`}>
      {!load && (
        <Tab
          tabs={["PROJETCS", "ADD NEW"]}
          setTab={setTab}
          active={tab}
          className={`w-75`}
        />
      )}
      {tab === "PROJETCS" && (
        <Project
          className={``}
          slide={isScreenPort(480) ? true : false}
          image={`${styles.image}`}
          setLoad={setLoad}
          admin={admin}
        />
      )}
      {tab === "ADD NEW" && <New />}
    </div>
  );
};

export const EditProjects = () => {
  return <div></div>;
};

export default Projects;
