import gql from "graphql-tag";

const ADD_PROJECTS = gql`
  mutation addProject(
    $cat: String!
    $con: String!
    $header: String!
    $caption: String!
    $image: [String!]!
  ) {
    updateProject(
      updateProjectInput: {
        category: $cat
        content: $con
        header: $header
        caption: $caption
        image: $image
      }
    ) {
      category
      caption
      header
      content
      id
      image
    }
  }
`;
const GET_ALL_PROJECTS = gql`
  query getAllProjects {
    findAllProjects {
      category
      content
      header
      id
      caption
      image
    }
  }
`;

const GET_SLIDER = gql`
  query slider {
    projectsSlider {
      caption
      category
      content
      image
      header
    }
  }
`;

export { GET_ALL_PROJECTS, ADD_PROJECTS, GET_SLIDER };
