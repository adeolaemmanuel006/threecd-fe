import Input from "../../../../components/input/input";
import Button from "../../../../components/button/button";
import { Tab } from "../../../../components/tabs/Tab";
import styles from "./new.module.css";
import { useMutation } from "@apollo/client";
import { ADD_PROJECTS } from "../projects.gql";
import { useState } from "react";

const New: React.FC = () => {
  const [addProjects, { loading, error }] = useMutation(ADD_PROJECTS);
  const [form, setForm] = useState<{
    cat: string;
    con: string;
    header: string;
    caption: string;
    id: number;
    image: string[];
  }>({
    cat: "",
    con: "",
    header: "",
    caption: "",
    id: 0,
    image: [],
  });
  const [allImages, setAllImages] = useState<string[]>([]);

  const submit = async (e: any) => {
    e.preventDefault();
    // console.log(form);
    if (
      form.header &&
      form.caption &&
      form.con &&
      form.cat &&
      form.image.length !== 0
    ) {
      const body = new FormData();
      body.append("upload_preset", "a0bekbbu");
      let data;
      for (const img of form.image) {
        body.append("file", img);
        const res = await fetch(
          `https://api.cloudinary.com/v1_1/${"threecdimesion"}/image/upload`,
          {
            method: "POST",
            body,
          }
        );
        data = await res.json();
        if (data.url) {
          allImages.push(data.url);
        }
      }
      form.image = allImages;
      const added = await addProjects({ variables: form });
      if (added.errors) {
        alert(added.errors);
      } else {
        alert("Data submited");
      }
    }
  };
  return (
    <div className={`${styles.new_con}`}>
      <form
        className={`${styles.form}`}
        id="addProject"
        onSubmit={(e) => submit(e)}
      >
        <div
          style={{
            display: "flex",
            flexFlow: "row wrap",
            justifyContent: "space-evenly",
          }}
        >
          <Input
            type="file"
            className={`w3-white w3-card w3-margin-bottom theme`}
            name="image"
            textChange={(file) => {
              const imag = [...form.image];
              if (imag.length <= 4) {
                imag.push(file);
                setForm({ ...form, image: imag });
              }
            }}
            placeholder={
              form.image.length === 0
                ? "Upload Image"
                : `Add More Image: ${4 - form.image.length} left of 4`
            }
          />
        </div>
        <Input
          type="text"
          className={`w3-card w3-input w3-round w3-border ${styles.input}`}
          placeholder="Header"
          name="header"
          textChange={(text) => setForm({ ...form, header: text })}
          maxLength={20}
        />
        <Input
          type="text"
          className={`w3-card w3-input w3-round w3-margin-top w3-border ${styles.input}`}
          placeholder="Caption"
          name="caption"
          textChange={(text) => setForm({ ...form, caption: text })}
          maxLength={10}
        />
        <Input
          type="select"
          className={`w3-card w3-input w3-round w3-margin-top w3-border ${styles.input}`}
          textChange={(text) => setForm({ ...form, cat: text })}
          selectData={[
            { value: "CATEGORIES", name: "CATEGORIES" },
            { value: Tab.cons, name: Tab.cons },
            { value: Tab.conrt, name: Tab.conrt },
            { value: Tab.consl, name: Tab.consl },
          ]}
          name="categories"
        />
        <Input
          type="textarea"
          className={`w3-card w3-input w3-round w3-margin-top w3-border ${styles.textarea}`}
          placeholder="Content"
          name="content"
          textChange={(text) => setForm({ ...form, con: text })}
        />
        <div className="w3-center" style={{ margin: "auto" }}>
          <Button
            className={`btn btn-success mt-3 w-50`}
            type="submit"
          >
            Submit
          </Button>
        </div>
      </form>
    </div>
  );
};
export default New;
