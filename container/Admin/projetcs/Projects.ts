import { Tab } from "../../../components/tabs/Tab";

export interface ProjectsDto {
  findAllProjects: PROJECTS[];
}
export interface SliderDto {
  projectsSlider: PROJECTS[];
}
export interface PROJECTS {
  id: number;
  category: Tab.conrt | Tab.consl | Tab.cons;
  header: string;
  caption: string;
  content: string;
  image: string[];
}
export interface SLIDER {
  id: number;
  category: Tab.conrt | Tab.consl | Tab.cons;
  header: string;
  caption: string;
  content: string;
  image: string;
}
