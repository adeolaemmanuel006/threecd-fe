import styles from "./about.module.css";
import { isScreenPort } from "../../../assets/scripts/utils";
import Button from "../../../components/button/button";
import { AboutI } from "../../About/About";
import Input from "../../../components/input/input";
import { useAppDispatch, useAppSelector } from "../../../store/store";
import { setModal } from "../../../store/reducers/pageSlice";
import AboutAdmin from "../../About/about";
import { useState } from "react";
import { gql, useMutation } from "@apollo/client";
import Loading from "../../../components/loading/loading";

export const ADD_TEAM = gql`
  mutation addTeam(
    $image: String!
    $name: String!
    $summary: String!
    $role: String!
  ) {
    addTeamMember(
      teamInput: { image: $image, name: $name, summary: $summary, role: $role }
    ) {
      id
    }
  }
`;

export const ADD_ABOUT = gql`
  mutation setAbout($about: About!) {
    setAbout(about: $about) {
      summary
      image
    }
  }
`;

export const AdminModal: React.FC<{ type: string }> = ({ type }) => {
  const [form, setForm] = useState({
    image: "",
    role: "",
    name: "",
    summary: "",
  });

  const [addTeam, {loading: teamLoading}] = useMutation(ADD_TEAM);
  const [addAbout, {loading: aboutLoading}] = useMutation(ADD_ABOUT);
  const [uploadedImage, setUploadedImage] = useState<boolean | string>(false);

  const aboutData = [
    {
      type: "about",
      data: [
        { type: "file", placeholder: "Upload Image", name: "image" },
        { type: "textarea", placeholder: "About...", name: "summary" },
      ],
    },
    {
      type: "team",
      data: [
        { type: "file", placeholder: "Upload Image", name: "image" },
        { type: "text", placeholder: "Name", name: "name" },
        { type: "text", placeholder: "Role", name: "role" },
        { type: "textarea", placeholder: "Summary...", name: "summary" },
      ],
    },
  ];

  const submit = async (e: any, type: string) => {
    e.preventDefault();
    setUploadedImage(true);
    // console.log(form);
    if (
      form.image &&
      form.role &&
      form.summary &&
      form.name &&
      type === "team"
    ) {
      const body = new FormData();
      body.append("file", form.image);
      body.append("upload_preset", "i5n0oohh");
      const res = await fetch(
        `https://api.cloudinary.com/v1_1/${"neutron360"}/image/upload`,
        {
          method: "POST",
          body,
        }
      );
      const data = await res.json();
      if (data.url) {
        form.image = data.url;
        setUploadedImage(form.image);
      }
      const added = await addTeam({
        variables: {
          image: form.image,
          name: form.name,
          role: form.role,
          summary: form.summary,
        },
      });
      if (added.data) {
        alert("Data Submited");
      }
    }
    if (form.summary && type === "about") {
      const body = new FormData();
      body.append("file", form.image);
      body.append("upload_preset", "qefgkhhb");
      const res = await fetch(
        `https://api.cloudinary.com/v1_1/${"threecdimesion"}/image/upload`,
        {
          method: "POST",
          body,
        }
      );
      const data = await res.json();
      if (data.url) {
        form.image = data.url;
      }
      const added = await addAbout({
        variables: {
          about: { image: form.image, summary: form.summary },
        },
      });
      if (added.data) {
        alert("Data Submited");
      }
    }
  };

  const modal = () => {
    if (type === "about") {
      return (
        <div className={`w3-container ${styles.con}`}>
          <form onSubmit={(e) => submit(e, "about")}>
            {aboutData[0].data?.map((data, ind) => {
              return (
                <div key={data.placeholder}>
                  <Input
                    name="aboutImage"
                    type={data.type}
                    key={ind}
                    placeholder={data.placeholder}
                    textChange={(text) => {
                      setForm({ ...form, [data.name]: text });
                    }}
                    style={{ marginTop: 80 }}
                    className={`${
                      data.type === "file"
                        ? "w3-btn w3-margin-bottom theme"
                        : `w3-input w3-border w3-round w3-margin-top`
                    } ${styles.input}`}
                  />
                </div>
              );
            })}
            <Button
            loading={aboutLoading}
              className="w3-btn theme w3-padding btn-success mx-auto d-block mt-3 w-50 text-white"
              type="submit"
            >
              Submit
            </Button>
          </form>
        </div>
      );
    }
    if (type === "team") {
      return (
        <div className={``}>
          <form
            className={`w3-container ${styles.con}`}
            style={{ marginTop: isScreenPort(480) ? "170px" : "" }}
            onSubmit={(e) => submit(e, "team")}
          >
            {aboutData[1].data?.map((data, ind) => {
              return (
                <Input
                  type={data.type}
                  key={ind}
                  placeholder={data.placeholder}
                  textChange={(text) => {
                    setForm({ ...form, [data.name]: text });
                  }}
                  name={data.placeholder}
                  className={`${
                    data.type === "file"
                      ? "w3-btn w3-margin-bottom theme"
                      : `w3-input w3-border w3-round w3-margin-top ${styles.input}`
                  }`}
                />
              );
            })}
            <div className="w3-center">
              <Button loading={teamLoading} className="btn-success w-50 mt-4" type="submit">
                Submit
              </Button>
            </div>
          </form>
        </div>
      );
    }
  };

  return <>{modal()}</>;
};

const About: React.FC<AboutI> = () => {
  const dispatch = useAppDispatch();
  const admin = useAppSelector((state) => state.page.admin);

  const page = (type: string) => {
    if (admin) {
      dispatch(
        setModal({
          isOpen: true,
          type: "standard",
          component: "AdminAbout",
          data: type,
        })
      );
    } else {
      dispatch(
        setModal({
          isOpen: true,
          type: "standard",
          component: "AdminAbout",
          data: type,
        })
      );
    }
  };

  return <AboutAdmin admin={admin} action={page} />;
};

export default About;
