import { useAppSelector } from "../../store/store";
import Slider from "./slider/slider";
import styles from "./home.module.css";
import { useEffect, useState } from "react";
import { Button } from "@mui/material";
import { HomeI } from "./Home";
import { GET_SLIDER } from "../Admin/projetcs/projects.gql";
import { SliderDto } from "../Admin/projetcs/Projects";
import { useQuery } from "@apollo/client";
import { motion } from "framer-motion";
import Loading from "../../components/loading/loading";
import { useRouter } from "next/router";

const Home: React.FC<HomeI> = ({}) => {
  const { data: projects, loading } = useQuery<SliderDto>(GET_SLIDER);
  const project = projects?.projectsSlider;
  let [index, setIndex] = useState(0);
  const [type, setType] = useState("play");
  const router = useRouter();
  let timer: NodeJS.Timer;

  const controls = controlsHandler();

  useEffect(() => {
    project && controls.start();
    return () => clearInterval(timer);
  }, [index, type]);

  function controlsHandler() {
    return {
      start: function (ind = index) {
        timer = setInterval(() => {
          if (type === "play") {
            if (ind !== 3 && ind >= 0) {
              ind += 1;
              setIndex(ind);
            } else {
              setIndex(0);
            }
          }
        }, 7000);
      },
      type: function () {
        if (type === "pause") {
          setType("play");
        } else {
          setType("pause");
        }
      },
      next: function (ind: number) {
        clearInterval(timer);
        if (ind !== 4 && ind >= 0) {
          setIndex(ind);
          this.start(ind);
        } else {
          setIndex(0);
          this.start(0);
        }
      },
    };
  }

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        project?.slice(0, 4).map((data, ind) => {
          if (ind === index) {
            return (
              <motion.div
                initial={{ scale: 0.5 }}
                animate={{ scale: 1 }}
                transition={{ stiffness: 1, bounceStiffness: 1 }}
                key={`${data.category}-${ind}`}
                className={`${styles.background}`}
                style={{ backgroundImage: `url(${data.image})` }}
              >
                <motion.h1
                  initial={{ y: "-50%", opacity: 0 }}
                  animate={{ y: 0, opacity: 1 }}
                  transition={{ delay: 0.5 }}
                  className={`${styles.counts} theme-2`}
                >
                  0{index + 1}
                </motion.h1>
                <motion.div
                  initial={{ y: "-50%", opacity: 0 }}
                  animate={{ y: 0, opacity: 1 }}
                  transition={{ delay: 0.8, duration: 0.5 }}
                  className={`${styles.works_con}`}
                >
                  <p className={`${styles.slider_cat} w3-text-white`}>
                    {data.category}
                  </p>
                  <h1 className={`${styles.header}`}>
                    {data?.header.toUpperCase()}
                  </h1>
                  <p className={`${styles.caption}`}>{data.caption}</p>
                  <Button
                    className={`${styles.btn} theme-3`}
                    variant="outlined"
                    color="inherit"
                    onClick={() => router.push("/projects")}
                  >
                    All Projects
                  </Button>
                </motion.div>
              </motion.div>
            );
          }
        })
      )}
      <Slider index={index} type={type} handler={controls} />
    </>
  );
};

export default Home;
