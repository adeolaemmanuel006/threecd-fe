import styles from "./slider.module.css";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import PauseIcon from "@mui/icons-material/Pause";
import { Button } from "@mui/material";
import { SliderI } from "./Slider";


const Slider: React.FC<SliderI> = ({ index, type, handler }) => {
  return (
    <div className={`${styles.flex}`}>
      <div className={`${styles.counter} theme-2`}>
        {[1, 2, 3, 4].map((data, ind) => {
          return (
            <div
              className={`${styles.counter_circle} ${
                index === ind && "theme-3"
              }`}
              key={data}
            ></div>
          );
        })}
      </div>
      <div className={`${styles.slider} theme`}>

      </div>
      <div className={`${styles.arrows} theme`}>
        <Button sx={{ color: "white" }} onClick={() => handler.next(index - 1)}>
          <ArrowBackIosIcon sx={{ width: 40, height: 40, marginTop: 1.5 }} />
        </Button>
        <Button sx={{ color: "white" }} onClick={() => handler?.type()}>
        {type === "pause" && (
          <PlayArrowIcon
            sx={{
              width: 50,
              height: 50,
              color: "white",
              marginTop: 1,
              cursor: "pointer",
            }}
          />
        )}
        {type === "play" && (
          <PauseIcon
            sx={{
              width: 50,
              height: 50,
              color: "white",
              marginTop: 1,
              cursor: "pointer",
            }}
          />
        )}
        </Button>
        <Button sx={{ color: "white" }} onClick={() => handler.next(index + 1)}>
          <ArrowForwardIosIcon sx={{ width: 40, height: 40, marginTop: 1.5 }} />
        </Button>
      </div>
    </div>
  );
};

export default Slider;
