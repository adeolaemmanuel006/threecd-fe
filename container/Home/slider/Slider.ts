export interface SliderI {
  index: number;
  loading?: number;
  type?: string;
  handler: {
    next: (ind: number) => void;
    type: () => void;
    start: (ind?: number) => void;
  };
}
