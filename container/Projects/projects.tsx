import { gql, useMutation, useQuery } from "@apollo/client";
import { AnimatePresence, motion } from "framer-motion";
import { useEffect, useState } from "react";
import { isScreenPort } from "../../assets/scripts/utils";
import Button from "../../components/button/button";
import Loading from "../../components/loading/loading";
import { setModal } from "../../store/reducers/pageSlice";
import { useAppDispatch, useAppSelector } from "../../store/store";
import { ProjectsDto } from "../Admin/projetcs/Projects";
import { GET_ALL_PROJECTS } from "../Admin/projetcs/projects.gql";
import styles from "./projects.module.css";

const DELETE_PROJECT = gql`
  mutation deleteProject($id: Int!) {
    removeProject(id: $id) {
      id
    }
  }
`;

const Projects: React.FC<{
  slide?: boolean;
  className?: any;
  image?: string;
  admin?: boolean;
  setLoad?: (data: boolean) => void;
}> = ({ slide, className, image, setLoad, admin }) => {
  const { data: projects, loading } = useQuery<ProjectsDto>(GET_ALL_PROJECTS);
  const projectDatas = projects?.findAllProjects;
  const dispatch = useAppDispatch();

  useEffect(() => {
    setLoad && setLoad(loading);
  }, [loading]);

  if (loading) {
    return <Loading />;
  }

  const projectHandler = (data: any) => {
    if (admin)
      dispatch(
        setModal({
          props: data,
          isOpen: true,
          type: "sidemodal",
          component: "ProjectModal",
        })
      );
    if (!admin)
      dispatch(
        setModal({
          props: data,
          isOpen: true,
          type: "sidemodal",
          component: "ProjectModal",
        })
      );
  };

  const scaleInitAnim = { scale: "0" };
  const xInitAnim = { x: "-100%" };
  const scaleFinalAnim = { scale: 1 };
  const xFinalAnim = { x: 0 };

  const AnimInitialData = isScreenPort(480) ? xInitAnim : scaleInitAnim;
  const AnimFinalData = isScreenPort(480) ? xFinalAnim : scaleFinalAnim;

  const variant = {
    initial: {
      ...AnimInitialData,
    },
    final: {
      ...AnimFinalData,
    },
  };

  return (
    <div className={`theme-light ${styles.project_con}`}>
      <AnimatePresence exitBeforeEnter>
        {projectDatas?.map((data, ind: number) => {
          return (
            <motion.div
              key={data.header}
              id={data.header}
              variants={variant}
              animate="final"
              initial="initial"
              transition={{ duration: 0.5, delay: 0.3 * ind }}
              exit={{ ...AnimInitialData }}
              className={`${styles.col}`}
            >
              <img
                src={data?.image[0]}
                className={`${image ? image : styles.image}`}
                onClick={() => projectHandler(data)}
              />
              <div
                className={`theme ${styles.project_details}`}
                onClick={() => projectHandler(data)}
              >
                <p className={`w3-text-white w3-padding ${styles.cat_text}`}>
                  {data?.category?.toUpperCase()}
                </p>
                <p className={`w3-text-white w3-padding ${styles.header_text}`}>
                  {data?.header?.substring(0, 1)?.toUpperCase() +
                    data?.header?.substring(1, data?.header?.length)}
                </p>
              </div>
              <div className={`theme ${styles.count}`}>
                <p className={`${styles.ind}`}>{(ind as number) + 1}</p>
              </div>
            </motion.div>
          );
        })}
      </AnimatePresence>
    </div>
  );
};

export const ProjectsModal: React.FC = () => {
  const data: any = useAppSelector((state) => state.page.modal.props);
  const [currImage, setCurrImage] = useState(data.image[0]);
  const admin = useAppSelector((state) => state.page.admin);
  const [deleteProject, { data: deletedProject }] = useMutation(DELETE_PROJECT);
  console.log(admin);

  const imageStyle = (image: any[]) => {
    return {
      flexBasis: `${98 / image.length}%`,
      marginTop: 10,
      cursor: "pointer",
    };
  };

  const deleteP = async (id: number) => {
    const response = await deleteProject({ variables: { id: id } });
    if (response.errors) {
      alert(response.errors);
    } else {
      alert("Data Deleted");
    }
  };

  return (
    <>
      <div
        style={{
          display: "flex",
          flexFlow: "row wrap",
          justifyContent: "space-around",
          overflowY: 'scroll'
        }}
      >
        <img src={currImage} className={`${styles.modal_image}`} />
        {data.image.map((img: string) => {
          return (
            <div style={{ ...imageStyle(data.image) }}>
              <img
                src={img}
                className={`${styles.modal_image_small}`}
                onClick={() => setCurrImage(img)}
              />
            </div>
          );
        })}
        <div className="w3-center theme-2-text">
          <span>{data.caption}</span>
          <h2 className="w3-bold">{data.header}</h2>
          <h6>{data.category}</h6>
          <p>{data.content}</p>
        </div>
      </div>
      {admin && (
        <div className="w3-center">
          <Button click={() => deleteP(data.id)}>
            <img src="/img/delete-bin.svg" />
            Delete
          </Button>
        </div>
      )}
    </>
  );
};

export default Projects;
