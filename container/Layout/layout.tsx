import React, { useState } from "react";
import { Stack } from "@mui/material";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import styles from "./layout.module.css";
import NavBar from "../Nav/navbar/navbar";
import Menu from "../Nav/menu/menu";
import { useAppDispatch, useAppSelector } from "../../store/store";
import Modal from "../../components/modal/modal";
import Tool from "../../components/tool/tool";
import { setNav } from "../../store/reducers/pageSlice";

const Layout: React.FC = ({ children }) => {
  const [sideBar, setSideBar] = useState(false);
  const isAdmin = useAppSelector((state) => state.page.admin);
  const modal = useAppSelector((state) => state.page.modal);
  const dispatch = useAppDispatch();

  const layoutActionHandler = () => {
    if (sideBar) {
      setSideBar(false);
      dispatch(setNav({ navIsOpen: false }));
    }
  };

  return (
    <>
      <Menu
        isOpen={sideBar}
        isOpenHandler={() => {
          if (sideBar) {
            setSideBar(false);
            dispatch(setNav({ navIsOpen: false }));
          } else {
            setSideBar(true);
            dispatch(setNav({ navIsOpen: true }));
          }
        }}
        isAdmin={isAdmin}
      />
      <NavBar
        openSideBar={() => {
          if (sideBar) {
            setSideBar(false);
            dispatch(setNav({ navIsOpen: false }));
          } else {
            setSideBar(true);
            dispatch(setNav({ navIsOpen: true }));
          }
        }}
      />
      {modal.isOpen && <Modal type={modal.type} />}
      <div
        className={`flex-row ${styles.overFlow}`}
        onClick={() => layoutActionHandler()}
      >
        <div className={`${styles.sidebar} theme-2`}>
          <Stack sx={{ alignItems: "center" }}>
            <div className={styles.vl}></div>
            <FacebookIcon
              sx={{ width: 40, height: 40, color: "white", margin: 2 }}
            />
            <TwitterIcon
              sx={{ width: 40, height: 40, color: "white", margin: 2 }}
            />
            <InstagramIcon
              sx={{ width: 40, height: 40, color: "white", margin: 2 }}
            />
          </Stack>
        </div>
        <div className={`${styles.main}`}>
          {children}
          {/* <Tool /> */}
        </div>
      </div>
    </>
  );
};

export default Layout;
