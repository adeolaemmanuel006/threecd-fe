import styles from "./about.module.css";
import { AboutI } from "./About";
import { isScreenPort } from "../../assets/scripts/utils";
import { useAppDispatch, useAppSelector } from "../../store/store";
import { motion } from "framer-motion";
import Button from "../../components/button/button";
import { gql, useMutation, useQuery } from "@apollo/client";
import { useState } from "react";
import { setModal } from "../../store/reducers/pageSlice";
import Input from "../../components/input/input";
import Loading from "../../components/loading/loading";
import Tab from "../../components/tabs/tabs";
const GET_TEAM = gql`
  query getTeam {
    getTeam {
      id
      image
      name
      role
      summary
    }
  }
`;

const GET_ABOUT = gql`
  query getAbout {
    getAbout {
      image
      summary
    }
  }
`;

const EDIT_TEAM = gql`
  mutation editTeam($team: TeamDto!) {
    editTeamMember(teamInput: $team) {
      id
      name
      image
      role
      summary
    }
  }
`;

interface TEAMS {
  id?: number;
  image: string;
  name: string;
  role: string;
  summary: string;
}

const About: React.FC<AboutI> = ({ admin, action }) => {
  const aboutData = useAppSelector((state) => state.page.aboutSection);
  const { loading: teamsLaoding, data: teams } =
    useQuery<{ getTeam: TEAMS[] }>(GET_TEAM);
  const { loading: aboutLoading, data: about } =
    useQuery<{ getAbout: { image: string; summary: string } }>(GET_ABOUT);
  const [edit, setEdit] = useState(false);
  const dispatch = useAppDispatch();
  const Teams = teams?.getTeam;
  const About = about?.getAbout;
  const [tab, setTab] = useState("About");

  if (teamsLaoding || aboutLoading) {
    return <Loading />;
  }

  const teamHandler = (data: TEAMS, type: string) => {
    if (type === "edit")
      dispatch(
        setModal({
          props: data,
          isOpen: true,
          component: "EditAboutModal",
          type: "sidemodal",
        })
      );
    if (type === "view")
      dispatch(
        setModal({
          props: data,
          isOpen: true,
          component: "AboutModal",
          type: "sidemodal",
        })
      );
  };

  const buttonAnim = {
    initial: { height: "0px", width: "0px" },
    animate: {
      height: "50px",
      width: "250px",
      transition: {
        duration: 0.5,
        delay: 0.5,
      },
    },
  };

  return (
    <>
      <Tab
        tabs={["About", "Team"]}
        active={tab}
        setTab={setTab}
        className={`w-50`}
      />
      {tab === "About" &&
        aboutData.map((data, ind) => {
          if (tab.startsWith("A") === data.header.startsWith("A")) {
            return (
              <div className={`${styles.about_con}`}>
                <div className="w3-center mt-4">
                  <p className={`${styles.sub_header}`}>
                    {data.subHeader.toUpperCase()}
                  </p>
                </div>
                <div className={`${styles.con}`}>
                  <p className={`${styles.about_text}`}>{About?.summary}</p>
                </div>
                {admin && (
                  <div className="w3-center">
                    <Button
                      className="btn-success mt-4 w-25"
                      click={() => action && action("about")}
                    >
                      Edit
                    </Button>
                  </div>
                )}
              </div>
            );
          }
        })}
      {tab === "Team" &&
        aboutData.map((data, ind) => {
          if (tab.startsWith("A") === data.header.startsWith("A")) {
            return (
              <div className={`${styles.about_con} w3-center mt-4`}>
                <p className={`${styles.sub_header}`}>
                  {data.subHeader.toUpperCase()}
                </p>
                <div className={`flex-row`}>
                  {Teams?.map((data) => {
                    return (
                      <>
                        <div
                          key={ind}
                          className={`row-col-30 ${styles.row_col_30} js-sb w3-display-container`}
                          onMouseOver={() => {
                            setEdit(true);
                          }}
                          onMouseLeave={() => {
                            setEdit(false);
                          }}
                        >
                          <img
                            className={`${styles.team_img}`}
                            src={data.image}
                          />
                          {edit && (
                            <Button
                              click={() =>
                                teamHandler(data, admin ? "edit" : "view")
                              }
                              className={`${styles.edit_btn} w3-display-bottommiddle mb-3 btn-primary theme`}
                            >
                              {admin ? "Edit" : data.name}
                            </Button>
                          )}
                        </div>
                      </>
                    );
                  })}
                </div>
                {admin && (
                  <div className="w3-center">
                    <Button
                      className="btn-success mt-4 w-25"
                      click={() => action && action("team")}
                    >
                      Add Team
                    </Button>
                  </div>
                )}
              </div>
            );
          }
        })}
    </>
  );
};

export const AboutModal = () => {
  const data: any = useAppSelector((state) => state.page.modal.props);

  return (
    <>
      <div
        style={{
          top: isScreenPort(480) ? 50 : "",
          left: isScreenPort(480) ? "30%" : "",
        }}
      >
        <img src={data.image} className={`${styles.team_img2}`} />
        <div className={`${styles.team_content}  theme-2-text w3-center`}>
          <h2 className="w3-bold">{data.name}</h2>
          <h6>{data.role}</h6>
          <p>{data.summary}</p>
        </div>
      </div>
    </>
  );
};

export const EditAboutModal = () => {
  const data: any = useAppSelector((state) => state.page.modal.props);
  const [editForm, setEditForm] = useState(data);
  const [editTeam, { data: editedTeam }] = useMutation(EDIT_TEAM);

  const submit = async (e: any) => {
    e.preventDefault();
    const body = new FormData();
    body.append("file", editForm.image);
    body.append("upload_preset", "qefgkhhb");
    const res = await fetch(
      `https://api.cloudinary.com/v1_1/${"threecdimesion"}/image/upload`,
      {
        method: "POST",
        body,
      }
    );
    const data = await res.json();
    if (data.url) {
      editForm.image = data.url;
    }
    const response = await editTeam({
      variables: {
        team: {
          id: editForm.id,
          image: editForm.image,
          name: editForm.name,
          role: editForm.role,
          summary: editForm.summary,
        },
      },
    });
    if (response.errors) {
      alert(response.errors);
    } else {
      alert("Data Submited");
    }
  };
  return (
    <>
      <form style={{ overflowY: "auto" }} onSubmit={(e) => submit(e)}>
        <img src={editForm.image} className={`${styles.edit_img}`} />
        <Input
          type="file"
          className={`w3-center w3-margin-top theme`}
          placeholder="Change image"
          name="editTeam"
          textChange={(image) => {
            setEditForm((state: any) => {
              return { ...state, image };
            });
          }}
        />
        <Input
          type="text"
          value={editForm.name}
          label="Name:"
          labelClass={`${styles.label} w3-bold`}
          className={`w3-input w3-border w3-round ${styles.input}`}
          textChange={(name) =>
            setEditForm((state: any) => {
              return { ...state, name };
            })
          }
        />
        <Input
          type="text"
          value={editForm.role}
          label="Role:"
          labelClass={`${styles.label} w3-bold`}
          className={`w3-input w3-border w3-round ${styles.input}`}
          textChange={(role) =>
            setEditForm((state: any) => {
              return { ...state, role };
            })
          }
        />
        <Input
          type="textarea"
          value={editForm.summary}
          label="Summary:"
          labelClass={`${styles.label} w3-bold`}
          className={`w3-input w3-border w3-round ${styles.textarea}`}
          textChange={(summary) =>
            setEditForm((state: any) => {
              return { ...state, summary };
            })
          }
        />
        <div className="w3-center">
          <Button type="submit" className={`btn-success mt-3 w-25`}>
            Submit
          </Button>
        </div>
      </form>
    </>
  );
};

export default About;
