import { gql } from "@apollo/client";

export interface ServiceDto {
  image: string;
  summary: string;
}

export const GET_SERVICE = gql`
  query getService($type: String!) {
    getServices(services: { type: $type }) {
      image
      summary
      created_at
      updated_at
    }
  }
`;
export const Add_Services = gql`
  mutation addServices($type: String!, $data: Service!) {
    addServices(servicesInput: { type: $type, data: $data }) {
      image
      summary
      created_at
      updated_at
    }
  }
`;
