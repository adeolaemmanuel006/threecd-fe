import styles from "./services.module.css";
import Tabs from "../../components/tabs/tabs";
import { Tab } from "../../components/tabs/Tab";
import { useEffect, useState } from "react";
import { isScreenPort } from "../../assets/scripts/utils";
import { useAppSelector } from "../../store/store";
import { useLazyQuery } from "@apollo/client";
import { GET_SERVICE, ServiceDto } from "./services.gql";
import { AnimatePresence, motion } from "framer-motion";
import {
  servicesCard,
  summary_animation,
  text_slideUp_animation,
} from "../../assets/animations/services";
import Button from "../../components/button/button";

const Services: React.FC<{
  admin?: boolean;
  action?: (data: any, type: string) => void;
}> = ({ admin, action }) => {
  const tabs = [Tab.cons, Tab.consl, Tab.conrt];
  const [tab, setTab] = useState(Tab.cons);
  const [getService, { data: services }] =
    useLazyQuery<{ getServices: ServiceDto }>(GET_SERVICE);
  const service = services?.getServices;

  const serviceData = useAppSelector((state) => state.page.serviceTab);

  useEffect(() => {
    getService({ variables: { type: tab } });
  }, [tab]);

  return (
    <div className={`theme-light ${styles.services_con}`}>
      <Tabs
        tabs={tabs}
        setTab={setTab}
        active={tab}
        className={`${isScreenPort(767) && "w-auto"}`}
      />

      {serviceData.map((cur, index) => {
        if (tab === cur.type) {
          return (
            <AnimatePresence key={cur.type}>
              <motion.div
                variants={servicesCard}
                initial="initial"
                animate="animate"
                exit="exit"
                className={`theme-light box_shadow ${styles.ser_con}`}
              >
                <motion.h4
                  className={`w3-center ${styles.serv_text} w3-bold p-2`}
                  style={{ color: "#264574" }}
                  variants={text_slideUp_animation}
                >
                  {cur.type}
                </motion.h4>
                <div className="">
                  {service?.image && <img className={`${styles.image}`} src={service?.image} />}
                  <motion.p
                    variants={summary_animation}
                    initial="initial"
                    animate="animate"
                    exit="exit"
                    style={{ color: "#264574", padding: 10 }}
                  >
                    {service?.summary}
                  </motion.p>
                </div>
                {admin && (
                  <div
                    className="w3-center mt-3"
                  >
                    <Button
                      className="theme btn-primary mb-2"
                      click={() => action && action(service, tab)}
                    >
                      Edit
                    </Button>
                  </div>
                )}
              </motion.div>
            </AnimatePresence>
          );
        }
      })}
    </div>
  );
};

export default Services;
